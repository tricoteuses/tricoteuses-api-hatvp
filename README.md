# Tricoteuses-API-HATVP

## Installation

```
apt-get install python3-psycopg2 csvkit
git clone https://framagit.org/tricoteuses/tricoteuses-api-hatvp
cd tricoteuses-api-hatvp/
```

Create a PostgresQL user and database

```bash
$ sudo -u postgres bash
$ psql
postgres=# CREATE USER hatvp WITH PASSWORD 'XXXXXXXX';
CREATE ROLE
postgres=# CREATE DATABASE hatvp;
CREATE DATABASE
postgres=# GRANT ALL PRIVILEGES ON DATABASE hatvp to hatvp;
GRANT
```

Create a `.env` file to set PostgreSQL database informations and other configuration variables (you can use `example.env` as a template). Then

```bash
npm install
```

Create a directory for temporary files:

```bash
mkdir ../hatvp-data
```

## Usage

```bash
$ npx babel-node --extensions ".ts" -- src/server.ts &
$ curl http://127.0.0.1:8002/repertoire?q=HAVAS
{
  "results": [
    {
      "typeIdentifiantNational": "SIREN",
      "denomination": "HAVAS PARIS",
...
      "dateCreation": "04/10/2019 17:31:42",
      "dateDernierePublicationActivite": "19/03/2019 17:15:03"
    }
  ]
}
```

## Sources

* https://www.hatvp.fr/le-repertoire/#open-data-repertoire links to http://www.hatvp.fr/agora/opendata/agora_repertoire_opendata.json
* https://www.hatvp.fr/consulter-les-declarations/#open-data links to https://www.hatvp.fr/files/open-data/liste.csv and https://www.hatvp.fr/livraison/merge/declarations.xml

