require("dotenv").config()

import { validateConfig } from "./validators/config"

const config = {
  DataGit: process.env.TRICOTEUSES_HATVP_DATA_GIT,
  DataDir: "../hatvp-data",
  HATVPApiUrl: process.env.TRICOTEUSES_HATVP_API_URL,
  gitlab: {
    accessToken: process.env.TRICOTEUSES_HATVP_GITLAB_ACCESS_TOKEN,
    groupPath: process.env.TRICOTEUSES_HATVP_GITLAB_GROUP_PATH,
    url: process.env.TRICOTEUSES_HATVP_GITLAB_URL,
    userEmail: process.env.TRICOTEUSES_HATVP_GITLAB_USER_EMAIL,
    username: process.env.TRICOTEUSES_HATVP_GITLAB_USERNAME,
  },
  db: {
    host: process.env.TRICOTEUSES_HATVP_DB_HOST,
    password: process.env.TRICOTEUSES_HATVP_DB_PASSWORD,
    port: process.env.TRICOTEUSES_HATVP_DB_PORT,
    user: process.env.TRICOTEUSES_HATVP_DB_USER,
  },
  listen: {
    host: process.env.TRICOTEUSES_HATVP_API_HOST,
    port: process.env.TRICOTEUSES_HATVP_API_PORT,
  },
}

const [validConfig, error] = validateConfig(config)
if (error !== null) {
  console.error(
    `Error in  configuration:\n${JSON.stringify(
      validConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validConfig
