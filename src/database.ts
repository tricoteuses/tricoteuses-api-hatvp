import PgPromise from "pg-promise"

import config from "./config"

const dbp = PgPromise()({
    database: "hatvp",
    host: config.db.host,
    password: config.db.password,
    port: config.db.port,
    user: config.db.user,
})

export let dbc : PgPromise.IConnected<unknown>

export async function checkDatabase() {
    dbc = await dbp.connect()
    return dbp
}
