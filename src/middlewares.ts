export const wrapAsyncMiddleware = (fn: (...args: any[]) => Promise<any>) => (
  ...args: any[]
) => fn(...args).catch(args[2])
// export const wrapAsyncMiddleware = fn => (...args) => fn(...args).catch(args[2])
