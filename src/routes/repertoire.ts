import { Request, Response } from "express"

import { dbc } from "../database"
import { wrapAsyncMiddleware } from "../middlewares"

export const queryRepertoire = wrapAsyncMiddleware(async function query(
  req: Request,
  res: Response,
) {
    const [query, error] = validateQuery(req.query)
    if (error !== null) {
        console.error(
            `Error in ${req.path}:\n${JSON.stringify(
             query,
             null,
             2,
           )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
        )
        res.writeHead(400, {
            "Content-Type": "application/json; charset=utf-8",
        })
        return res.end(
            JSON.stringify(
                {
                    ...query,
                    error: {
                        code: 400,
                        details: error,
                        message: "Invalid query",
                    },
                },
                null,
                2,
            ),
        )
    }
    const { q } = query

    let identifiers

    if (q) {
        identifiers = await dbc.any(
            `
          SELECT DISTINCT i.type_identifiant_national,i.identifiant_national
          FROM public."1_informations_generales" as i INNER JOIN public."3_collaborateurs" as c ON i.representants_id = c.representants_id
                                                      INNER JOIN public."2_dirigeants" as d ON i.representants_id = d.representants_id
          WHERE i."nom_usage_HATVP" ILIKE '%${q}%' OR
                i.denomination ILIKE '%${q}%' OR
                i."sigle_HATVP" ILIKE '%${q}%' OR
                c.nom_prenom_collaborateur ILIKE '%${q}%' OR
                d.nom_prenom_dirigeant ILIKE '%${q}%'
        `,
        )
    } else {
        identifiers = await dbc.any(
            `
          SELECT DISTINCT type_identifiant_national,identifiant_national FROM public."1_informations_generales" LIMIT 1
            `,
        )
    }

    if (identifiers.length == 0) {
        return res.end(
            JSON.stringify(
                {
                    ...query,
                    error: {
                        code: 404,
                        details: `${q} Not Found`,
                        message: "Not Found",
                    },
                },
                null,
                2,
            ),
        )
    }

    const identifiers_list = identifiers.map(({ type_identifiant_national,identifiant_national }:{[key: string]: any}) => `${type_identifiant_national}/${identifiant_national}`)

    const rows = await dbc.any(
        `
      SELECT description
      FROM public."agora_repertoire_opendata"
      WHERE identifiant IN ($<identifiers_list:list>)
        `,
        {
            identifiers_list,
        },
    )

    res.writeHead(200, {
        "Content-Type": "application/json; charset=utf-8",
    })
    res.end(
        JSON.stringify(
            {
                results: rows.map(({ description } : {[key:string]:any}) => JSON.parse(description)),
            },
            null,
            2,
        ),
    )
})

function validateQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "q"
    remainingKeys.delete(key)
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
