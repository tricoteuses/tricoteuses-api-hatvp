import http from "http"
import express from "express"
import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import fetch from "node-fetch"
import path from "path"
import StreamZip from "node-stream-zip"
import rimraf from "rimraf"
import child from "child_process"

import config from "./config"
import { dbc, checkDatabase } from "./database"
import { queryRepertoire } from "./routes/repertoire"

const optionsDefinitions = [
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function reset_database() {
    await reset_database_agora_repertoire_opendata()
    await reset_database_vues_separees_csv()
}

async function reset_database_agora_repertoire_opendata() {
    await dbc.none(`DROP TABLE IF EXISTS public."agora_repertoire_opendata"`)
    await dbc.none(
        `CREATE TABLE public."agora_repertoire_opendata" (
           identifiant VARCHAR(255) PRIMARY KEY,
           description TEXT NOT NULL
         )
        `)
    const p = path.join(config.DataDir, "agora_repertoire_opendata.json")
    const obj = JSON.parse(fs.readFileSync(p, "utf8"))
    for (const row of obj["publications"]) {
        const identifiant = `${row["typeIdentifiantNational"]}/${row["identifiantNational"]}`
        if (options.verbose)
            console.log(`${identifiant} ${row["denomination"]}`)
        const description = JSON.stringify(row)
        await dbc.none(`INSERT INTO public."agora_repertoire_opendata" (
                            identifiant,
                            description
                        ) VALUES (
                            $<identifiant>,
                            $<description>
                        )`, { identifiant, description })
    }
}

async function reset_database_vues_separees_csv() {
    let db = `postgresql://${config.db.user}:${config.db.password}@${config.db.host}:${config.db.port}/hatvp`
    let d = path.join(config.DataDir, "Vues séparées")
    const files = fs.readdirSync(d).filter(f => f.endsWith(".csv"))
    let p = child.spawnSync("csvsql", [
        "--overwrite", "-d;", "--db", db, "--insert",
    ].concat(files), {
        cwd: d,
    })
    if (p.status != 0) {
        console.log(p.stdout.toString("utf8"))
        console.log(p.stderr.toString("utf8"))
    }
    assert.strictEqual(p.status, 0)
    if (options.verbose)
        console.log("database reset")
}

async function download() {
    // if the download of any files fails or the files are corrupt, do not clobber existing files

    const zip = await download_file("https://www.hatvp.fr/agora/opendata/csv/Vues_Separees_CSV.zip")
    validate_zip(`${zip}.temp`)

    const json = await download_file("https://www.hatvp.fr/agora/opendata/agora_repertoire_opendata.json")
    JSON.parse(fs.readFileSync(`${json}.temp`, "utf8"))

    fs.renameSync(`${json}.temp`, json)
    fs.renameSync(`${zip}.temp`, zip)
}

async function download_safe() {
    let d = download()
    d.catch(error => {
        console.log(error)
    })
    try {
        await d
        return true
    } catch {
        console.log('ignore failed download')
        return false
    }
}

async function download_file(url: string) {
    if (options.verbose)
        console.log(`Retrieving ${url}`)

    let u = new URL(url).toString()
    const response = await fetch(u)
    const pageBuffer = Buffer.from(new Uint8Array(await response.arrayBuffer()))
    if (!response.ok) {
        throw new Error(
            `Error while getting page "${url}":\n\nError:\n${JSON.stringify(
           { code: response.status, message: response.statusText },
           null,
           2,
        )}`,
        )
    }
    const p = path.join(config.DataDir, path.basename(url))
    fs.writeFileSync(`${p}.temp`, pageBuffer)
    return p
}

async function validate_zip(pathname: string) {
    const zip = new StreamZip({
        file: pathname,
        storeEntries: true,
    })
    await new Promise(() => {
        zip.on("ready", () => {
            zip.close()
        })
    })
}

async function extract_vues_separees_csv() {
    const zip = new StreamZip({
        file: path.join(config.DataDir, "Vues_Separees_CSV.zip"),
        storeEntries: true,
    })
    rimraf.sync(path.join(config.DataDir, "Vues séparées"))
    await new Promise((resolve, reject) => {
        zip.on("ready", () => {
            zip.extract(null, config.DataDir, (err: any, _count: number) => {
                if (err)
                    console.log(`Extract error ${err}`)
                else if (options.verbose)
                    console.log(`Extracted ${_count} entries`)
                zip.close()
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            })
        })
    })
}

async function startExpress() {
    const host = config.listen.host
    const port = config.listen.port
    const app = express()
    const { NODE_ENV } = process.env
    const dev = NODE_ENV === "development"
    // Enable Express case-sensitive and strict options.
    app.enable("case sensitive routing")
    app.enable("strict routing")
    if (dev) {
        // Add logging.
        const morgan = require("morgan")
        app.use(morgan("dev"))
    }
    app.get("/repertoire", queryRepertoire)
    const server = http.createServer(app)
    await server.listen(port, host, () => {
        console.log(`Listening on ${host}:${port}...`)
    })
}

async function main() {
    if (await download_safe()) {
        await extract_vues_separees_csv()
    }
    await reset_database()
    await startExpress()
}

checkDatabase()
    .then(main)
    .catch(error => {
        console.log(error)
        process.exit(1)
    })
