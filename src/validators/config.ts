import {
  validateChain,
  validateInteger,
  validateMaybeTrimmedString,
  validateMissing,
  validateNonEmptyTrimmedString,
  validateNumber,
  validateOption,
  validateString,
  validateStringToNumber,
  validateTest,
  validateUrl,
} from "@biryani/core"

export function validateConfig(data: any): [any, any] {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  remainingKeys.delete("DataDir")
  for (const key of ["HATVPApiUrl", "DataGit"]) {
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "gitlab"
    remainingKeys.delete(key)
    const [value, error] = validateGitlab(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "listen"
    remainingKeys.delete(key)
    const [value, error] = validateListen(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "db"
    remainingKeys.delete(key)
    const [value, error] = validateDb(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateDb(data: any): [any, any] {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["host", "password", "user"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "port"
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateOption([validateString, validateStringToNumber], validateNumber),
      validateInteger,
      validateTest(
        (value: any) => 0 <= value && value <= 65535,
        "Must be an integer between 0 and 65535",
      ),
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateGitlab(data: any): [any, any] {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["accessToken", "groupPath", "userEmail", "username"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListen(data: any): [any, any] {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  {
    const key = "host"
    remainingKeys.delete(key)
    const [value, error] = validateMaybeTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "port"
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateOption(
        validateMissing,
        [validateString, validateStringToNumber],
        validateNumber,
      ),
      validateOption(validateMissing, [
        validateInteger,
        validateTest(
          (value: any) => 0 <= value && value <= 65535,
          "Must be an integer between 0 and 65535",
        ),
      ]),
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
